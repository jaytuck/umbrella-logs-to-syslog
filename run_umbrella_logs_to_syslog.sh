#!/bin/bash

cd /root/logging/umbrella_logs/
# needed if running behind a web proxy
export HTTPS_PROXY=http://example.com:3128

python3 umbrella_logs_to_syslog.py --config config.toml --debug


