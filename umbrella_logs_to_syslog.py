import subprocess, json, tempfile, argparse, sqlite3, signal, gzip, logging, logging.handlers, csv, json, toml, re
from functools import partial
from pathlib import Path
from time import sleep


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, help="configuration file to use")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--bind-path', help="bind to a unix socket, for testing")
    args = parser.parse_args()

    # Load in the TOML config
    config = toml.load(args.config)

    ### Configure stdout logging
    otherlogger = logging.getLogger('umbrelladebug')
    h = logging.StreamHandler()
    f = logging.Formatter("%(name)s == %(levelname)s == %(module)s:%(filename)s == %(asctime)s == %(message)s", datefmt='%Y-%m-%dT%H:%M%z')
    h.setFormatter(f)
    otherlogger.addHandler(h)
    global debug, info
    debug = otherlogger.debug
    info = otherlogger.info

    if args.debug:
        otherlogger.setLevel('DEBUG')
        debug(f'running with args: {args}')
        debug(f'running with config: {config}')
    else:
        otherlogger.setLevel('INFO')

    ### Configure syslog outputter
    # Validate the timezone
    if not re.match(r'^[-+]\d{2}:\d{2}$', config['timezone_format']):
        print(f'timezone format from config is not valid: {config["timezone_format"]} - must be in the numeric form like +01:30 or -01:30')
        exit(1)

    # https://www.rfc-editor.org/rfc/rfc5424 format
    # note that the priority if emitted by the syslog handler pre-formatter
    # Format is: VERSION SP TIMESTAMP SP HOSTNAME SP APP-NAME SP PROCID SP MSGID
    # We set PROCID and MSGID to NILVALUE aka -
    syslog_formatter = logging.Formatter(f"1 %(asctime)s {config['hostname']} umbrellalogs - - - %(message)s", datefmt=f'%Y-%m-%dT%H:%M:%S{config["timezone_format"]}')

    # bind_path can be used for testing the syslog formatting
    if args.bind_path:
        info(f'binding to path {args.bind_path}')
        syslog_handler = logging.handlers.SysLogHandler(address=args.bind_path, facility='local1')
    else:
        syslog_handler = logging.handlers.SysLogHandler(address=(config['destination_address'],config['destination_port']), facility=config['syslog_facility'])
    syslog_handler.setFormatter(syslog_formatter)

    global syslogger
    syslogger = logging.getLogger('syslogger')
    syslogger.setLevel('INFO')
    syslogger.addHandler(syslog_handler)


    # mount the Umbrella AWS storage with rclone
    mountpoint = tempfile.mkdtemp()
    rclone_proc = subprocess.Popen(['rclone','mount', '--dir-cache-time=15s', config['base_path'], mountpoint])
    file_base = Path(mountpoint)
    make_sure_rclone_mounted(file_base)

    try:
        debug(f'connecting to sqlite db {config["database"]}')
        db = sqlite3.connect(config["database"])
        # Create the files table if not already created
        init_db(db)

        while True:
            debug(f'getting a list of files to check')
            files = get_latest_files(file_base)
            debug(f'found {len(files)} files')
            for file in files:
                if not already_emitted(file, db):
                    debug(f'file {file.name} not already emitted to syslog, emitting now')
                    rows = emit_file(file, db)
                    debug(f'file {file.name} emitted {rows} lines')
                else:
                    #debug(f'file {file.name} already emitted to syslog, skipping')
                    pass

            cleanup_database(files, db)
            # for dev
            if args.debug:
                #break
                pass
            
            sleep(15)


    finally:
        # unmount rclone filesystem when completed
        rclone_proc.send_signal(signal.SIGINT)
        try:
            print(f'rclone exited with returncode: {rclone_proc.wait(30)}')
        except subprocess.TimeoutExpired:
            print(f'timed out waiting for rclone to finish')


def emit_file(file, db):
    with gzip.open(file, 'rt') as f:
        # see https://docs.umbrella.com/deployment-umbrella/docs/log-formats-and-versioning
        # for list of column types
        fieldnames = [
            'timestamp',
            'most_granular_identity',
            'identities',
            'internal_ip',
            'external_ip',
            'action',
            'query_type',
            'response_code',
            'domain',
            'categories',
            'most_granular_identity_type',
            'identity_types',
            'blocked_categories',
        ]
        reader = csv.DictReader(f, fieldnames=fieldnames)
        for i, row in enumerate(reader):
            row['domain'] = row['domain'].removesuffix('.')  # Clean up the implicit root '.' domain on the end of the domains
            #print(f'row: {row}')
            syslogger.info(json.dumps(row))
    cur = db.execute('INSERT INTO files values (?,?)', (file.name, 1))
    db.commit()
    return (i+1)  # number of rows that were processed


def cleanup_database(files, db):
    '''remove all the entries from the database that aren't in the current date-dir'''
    file_list = set(map(lambda f: f.name, files))
    for db_file_name in db.execute('SELECT name from files'):
        if db_file_name[0] not in file_list:
            debug(f'removing {db_file_name} from database')
            db.execute('DELETE FROM files WHERE name=?', (db_file_name[0],))
    db.commit()


def get_latest_files(file_base):
    # get dated dirs
    dirs = list(sorted(file_base.iterdir()))
    # list the files in the most recent dir, sorted by name to make them output in approximately correct order
    files = list(sorted(dirs[-1].iterdir()))
    return files


def already_emitted(file, db):
    cur = db.execute('SELECT name, finished from files where name=?', (file.name,))
    if cur.fetchone():
        return True
    else:
        return False


def make_sure_rclone_mounted(file_base):
    sleep(1)
    for i in range(4):
        dirs = list(file_base.iterdir())
        if dirs:
            continue
        else:
            sleep(1)
    if not dirs:
        raise Exception(f'failed to mount rclone on {file_base} : {dirs}')


def init_db(db):
    db.execute('''CREATE TABLE IF NOT EXISTS files(name, finished)''')
    db.commit()


if __name__ == "__main__":
    main()

