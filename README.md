# Setup Process

* install rclone from rclone website
* install dnf packages: fuse python3-toml
* configure the rclone remote
* check config file and update as needed
* test run script in debug mode with `python3 umbrella_logs_to_syslog.py --config config.toml --debug`
* install systemd service
* test running systemd service
* enable systemd service

This process was done on a Red Hat 9 server

### Install Rclone

Go to https://rclone.org/install/ and follow the instructions

### Install DNF packages

run `dnf install fuse python3-toml`

### Configure the rclone remote

run `rclone config` and follow the steps to create a new remote. When complete, you can check your settings with the command `cat ~/.config/rclone/rclone.conf` - this is ours with the remote name `umbrella`

```
root@server ~# cat ~/.config/rclone/rclone.conf
[umbrella]
type = s3
provider = AWS
access_key_id = ABC123...
secret_access_key = abc123...
region = ap-southeast-2
```
### Check/Update the config file

Make any required changes to the `config.toml` file. The S3 bucket path can be retrieved from the Umbrella Dashboard:

![Umbrella Dashboard Log Settings.png](Umbrella Dashboard Log Settings.png)

You can confirm the S3 bucket path using the `rclone lsd` command like this:
```
root@server ~/l/umbrella_logs (master)# rclone lsd umbrella:cisco-managed-ap-southeast-2/1234567_1234432112344321abcddefgabcd/dnslogs
           0 2023-07-15 14:31:47        -1 2023-07-06
           0 2023-07-15 14:31:47        -1 2023-07-07
           0 2023-07-15 14:31:47        -1 2023-07-08
           0 2023-07-15 14:31:47        -1 2023-07-09
           0 2023-07-15 14:31:47        -1 2023-07-10
           0 2023-07-15 14:31:47        -1 2023-07-11
           0 2023-07-15 14:31:47        -1 2023-07-12
           0 2023-07-15 14:31:47        -1 2023-07-13
           0 2023-07-15 14:31:47        -1 2023-07-14
           0 2023-07-15 14:31:47        -1 2023-07-15
```

